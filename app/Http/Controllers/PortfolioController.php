<?php

namespace App\Http\Controllers;

use App\Http\Requests\PortfolioRequest;
use App\Services\Portfolio\Portfolio;
use Illuminate\Http\Request;

class PortfolioController extends Controller
{
    //
    protected $portfolioService;

    public function __construct(Portfolio $portfolio)
    {
        $this->portfolioService = $portfolio;
    }

    public function index()
    {
        $result = $this->portfolioService->index();

        // dd($result['data']);
        return view('pages.portfolio.index', [
            'title' => 'Portfolio',
            'data'  => $result['data'],
        ]);
    }

    public function form()
    {
        return view('pages.portfolio.form', [
            'title' => 'Add Portfolio',
        ]);
    }

    public function store(PortfolioRequest $request)
    {
        $data = collect(request()->all())->filter()->all();

        $result = $this->portfolioService->store($data);

        return redirect('portfolio')->withSuccess($result['message']);
    }

    public function show($id)
    {
        $result = $this->portfolioService->show($id);
        return view('pages.portfolio.form', [
            'title' => 'Edit Portfolio',
            'data'  => $result['data'],
        ]);
    }

    public function update(PortfolioRequest $request, $id)
    {
        $data = collect(request()->all())->filter()->all();

        $result = $this->portfolioService->update($data, $id);

        return redirect(
            'portfolio'
        )->withSuccess(
            $result['message']
        );
    }

    public function delete($id)
    {
        $result = $this->portfolioService->delete($id);
        return response()->json($result);
        // return redirect('portfolio')->withSuccess($result['message']);
    }
}
