<?php

namespace App\Services\Portfolio;

use App\Models\Portfolio as ModelsPortfolio;
use Illuminate\Support\Facades\File;
use Vinkla\Hashids\HashidsManager;

class Portfolio
{
    //
    protected $hashids;

    public function __construct(HashidsManager $hashids)
    {
        $this->hashids = $hashids;
    }

    public function index()
    {
        $data = ModelsPortfolio::all();
        $status = true;
        $message = 'Success';

        $result = [
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];

        return $result;
    }

    public function store($data)
    {

        if (array_key_exists('photo', $data)) {
            $portfolio = $data['photo'];
            $fileName_portfolio = 'assets/portfolio/' . md5($portfolio->getClientOriginalName() . time()) . "." . $portfolio->getClientOriginalExtension();
            $portfolio->move('./uploads/assets/portfolio/', $fileName_portfolio);

            $data['photo'] = $fileName_portfolio;
        } else {
            $data['photo'] = 'default.png';
        }

        // dd($data);
        ModelsPortfolio::create($data);

        $status = true;
        $message = 'Success added portfolio';

        $result = [
            'status' => $status,
            'message' => $message,
        ];

        return $result;
    }

    public function show($id)
    {
        $id = collect($this->hashids->decode($id))->first();

        $data = ModelsPortfolio::findOrFail($id);

        $status = true;
        $message = 'Success';

        $result = [
            'status' => $status,
            'message' => $message,
            'data' => $data,
        ];

        return $result;
    }

    public function update($data, $id)
    {
        // dd($data);
        $res = ModelsPortfolio::find($id);

        if (array_key_exists('photo', $data)) {
            File::delete('uploads/' . $res['photo']);

            $portfolio = $data['photo'];
            $fileName_portfolio = 'assets/portfolio/' . md5($portfolio->getClientOriginalName() . time()) . "." . $portfolio->getClientOriginalExtension();
            $portfolio->move('./uploads/assets/portfolio/', $fileName_portfolio);

            $data['photo'] = $fileName_portfolio;
        }

        $res->update($data);

        $status = true;
        $message = 'Success updated portfolio';

        $result = [
            'status' => $status,
            'message' => $message,
        ];

        return $result;
    }

    public function delete($id)
    {
        $res = ModelsPortfolio::find($id);

        File::delete('uploads/' . $res['photo']);

        $res->delete();

        $status = true;
        $message = 'Success deleted portfolio';

        $result = [
            'status' => $status,
            'message' => $message,
        ];

        return $result;
    }
}
